//Proyect TechU - JVMP

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var requestjson = require('request-json');
var urlMLabRaiz = "https://api.mlab.com/api/1/databases/jmartinez/collections/"
var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMLabRaiz="";

var bodyParser = require('body-parser');
var bcrypt = require('bcrypt');
var dateFormat = require('dateformat');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
var BCRYPT_SALT_ROUNDS = 12;

app.use(function(req, res, next){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Header", "Origin, X-Requested-With, Content-Type, Accept");
  next();
})
app.listen(port);

//console.log('todo list RESTful API server started on: ' + port);

//Alta de usuarios
app.post('/v1/usuarios', function(req, res){
  //console.log(req.body);

  var query = 'q={"idcuenta":' + req.body.cuentaAlta +'}';

  clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "Cuentas?" + apiKey + "&" + query);
  clienteMLabRaiz.get('', function(err, resM, body){
    if(!err){
      if(body.length >= 1){
        res.status(400).send({"mensaje":"La cuenta proporcionada ya está relacionada, favor de validar"});
      }
      else{
        var queryU = 'q={"email":"' + req.body.emailAlta +'"}';
        var clienteMLabRaizU = requestjson.createClient(urlMLabRaiz + "Usuarios?" + apiKey + "&" + queryU);
        //console.log(urlMLabRaiz + "Usuarios?" + apiKey + "&" + queryU);
        clienteMLabRaizU.get('', function(err, resM, body){
          if(!err){
            if(body.length >= 1){
              res.status(400).send({"mensaje":"El usuario proporcionado ya existe, favor de validar"});
            }
            else{
              var querySeq = 's={"idcliente":-1}&l=1';
              var idclienteMax = 0;
              var clienteMLabRaizSeq = requestjson.createClient(urlMLabRaiz + "Usuarios?" + apiKey + "&" + querySeq);

              clienteMLabRaizSeq.get('',function(err,resM,body){
                if(!err){
                  idclienteMax = body[0] == null ? 1 : (body[0].idcliente+1);

                  bcrypt.hash(req.body.passwordAlta, BCRYPT_SALT_ROUNDS, function(err, hash) {
                    var jsonUsuairo = '{"nombre":"' + req.body.nombreAlta +'", "apellido":"' + req.body.apellidoAlta +'","email":"'
                    + req.body.emailAlta + '", "password":"' + hash + '", "idcliente":' + idclienteMax + '}';

                    var clienteMLabRaizInsert = requestjson.createClient(urlMLabRaiz + "Usuarios?" + apiKey);
                    clienteMLabRaizInsert.post('', JSON.parse(jsonUsuairo), function(err, resM, bodyInsert){
                      res.send(bodyInsert);
                    });

                    var jsonCuenta = '{"idcuenta":' + req.body.cuentaAlta +', "tipocuenta":"' + req.body.tipoCuentaAlta +'", "cliente":' + idclienteMax + '}';

                    var clienteMLabRaizCuenta = requestjson.createClient(urlMLabRaiz + "Cuentas?" + apiKey);
                    clienteMLabRaizCuenta.post('', JSON.parse(jsonCuenta), function(err, resM, bodyCuenta){
                      //Se crea la cuenta
                    });
                  })
                }
                else{
                  res.status(404).send('Intente mas tarde');
                }
              })
            }
          }
        })
      }
    }
  })
})

//Login de la aplicación usuario y password como parametros
app.post('/v1/login', function(req, res){
  var email = req.body.email;
  var password = req.body.password == null ? "" : req.body.password;
  var query = 'q={"email":"' + email +'"}';
  //console.log(query);

  clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "Usuarios?" + apiKey + "&" + query);
  clienteMLabRaiz.get('', function(err, resM, body){
    if(!err){
      if(body.length == 1){
        if(bcrypt.compareSync(password, body[0].password)){
          res.status(200).send({idcliente: body[0].idcliente, nombre: body[0].nombre});
        }
        else{
          res.status(400).send('Usuario o password incorrecto');
        }
      }
      else{
        res.status(400).send('Usuario o password incorrecto');
      }
    }
  })
})

//Guarda una cuenta relacionado a un usuario
app.post('/v1/clientes/:idcliente/cuentas', function(req, res){
  var idcliente = req.params.idcliente;
  var query = 'q={"idcuenta":' + req.body.asociarCuentaAlta +'}';

  clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "Cuentas?" + apiKey + "&" + query);
  clienteMLabRaiz.get('', function(err, resM, body){
    if(!err){
      if(body.length >= 1){
        res.status(400).send({"mensaje":"La cuenta proporcionada ya está relacionada, favor de validar"});
      }
      else{
        var jsonCuenta = '{"idcuenta":' + req.body.asociarCuentaAlta +', "tipocuenta":"' + req.body.asociarTipoCuentaAlta +'", "cliente":' + idcliente + '}';

        var clienteMLabRaizCuenta = requestjson.createClient(urlMLabRaiz + "Cuentas?" + apiKey);
        clienteMLabRaizCuenta.post('', JSON.parse(jsonCuenta), function(err, resM, bodyCuenta){
          res.status(201).send({"mensaje":"La cuenta proporcionada fue relacionada con éxito"});
        });
      }
    }
  });
})

//Obtiene las cuentas de un usuario
app.get('/v1/clientes/:idcliente/cuentas', function(req, res){
  var idcliente = req.params.idcliente;
  var query = 'q={"cliente":' + idcliente +'}';

  clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "Cuentas?" + apiKey + "&" + query);
  clienteMLabRaiz.get('', function(err, resM, body){
    if(!err){
      if(body.length > 0){
        res.status(200).send(body);
      }
      else{
        res.status(400).send('No se encontraron cuentas asociandas a su usuario');
      }
    }
  });
})

//Crea un movimiento a una cuenta asociada
app.post('/v1/clientes/:idcliente/cuentas/:idcuenta/movimientos', function(req, res){
  var idcliente = req.params.idcliente;
  var idcuenta = req.params.idcuenta;
  var importe = req.body.importeAlta;
  var tipo = req.body.tipoMovimientoAlta;
  var query = 'q={"cliente":' + idcliente +', "idcuenta": ' + idcuenta +'}';

  clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "Cuentas?" + apiKey + "&" + query);
  clienteMLabRaiz.get('', function(err, resM, body){
    if(!err){
      if(body.length == 1){
        var now = new Date();
        var fechaMovimiento = dateFormat(now, "yyyy-mm-dd HH:MM:ss");
        var idMovimiento = body[0].movimientos == null ? 1 : (body[0].movimientos.length+1);
        var movimiento = {
            id: idMovimiento,
            importe: importe,
            tipo: tipo,
            fecha: fechaMovimiento
        };

        var data = [];
        data = body[0].movimientos == null ? [] : body[0].movimientos;
        data.push(movimiento)
        body[0].movimientos = data;
        var jsonCuentaMovimiento = body[0];

        var clienteMLabRaizMovimiento = requestjson.createClient(urlMLabRaiz + "Cuentas?" + apiKey);
        clienteMLabRaizMovimiento.post('', jsonCuentaMovimiento, function(err, resM, bodyCuentaMovimiento){
          //console.log(bodyCuentaMovimiento);
          res.status(200).send(bodyCuentaMovimiento);
        });
      }
      else{
        res.status(400).send('Error al generar el movimiento');
      }
    }
  })
})
