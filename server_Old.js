//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

var requestjson = require('request-json');

var movimientosJSON = require('./movimientosv2.json');

var urlClientesMLab = "https://api.mlab.com/api/1/databases/jmartinez/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

var urlMLabRaiz = "https://api.mlab.com/api/1/databases/jmartinez/collections/"
var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMLabRaiz;

var clienteMLab = requestjson.createClient(urlClientesMLab);

var bodyparse = require('body-parser');
app.use(bodyparse.json());
app.use(function(req, res, next){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Header", "Origin, X-Requested-With, Content-Type, Accept");
  next();
})


app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/',function(req, res){
  res.sendFile(path.join(__dirname, 'index.html'));
})

app.post('/', function (req, res){
  res.send('Hola recibido su peticion post cambiada');
})

app.put('/', function (req, res){
  res.send('Hola recibido su peticion put');
})

app.delete('/', function (req, res){
  res.send('Hola recibido su peticion delete');
})

app.get('/Clientes/:idcliente',function(req, res){
  res.send('Aqui tiene al cleinte numero ' + req.params.idcliente);
})

app.get('/Movimientos', function(req, res){
  res.sendfile('movimientosv1.json');
})

app.get('/v2/Movimientos', function(req, res){
  res.json(movimientosJSON);
})

//regresa el valor de la posision n del arreglo del json
app.get('/v2/Movimientos/:id', function(req, res){
  console.log(req.params.id);
  res.send(movimientosJSON[req.params.id-1]);
})

//peticion con query
app.get('/v2/Movimientosq', function(req, res){
  console.log(req.query);
  res.send('Query recibido' + req.query );
})

app.post('/v2/Movimientos', function (req, res){
  var nuevo = req.body;
  nuevo.id = movimientosJSON.length+1;
  movimientosJSON.push(nuevo);
  res.send('Movimiento dado de alta');
})

//usando la libreria request-json
app.get('/v1/Clientes', function(req, res){
  clienteMLab.get ('', function(err, resM, body){
    if(err) {
      console.log(body);
    }
    else{
      res.send(body);
    }
  })
})

app.post('/v1/Clientes', function(req, res){
  clienteMLab.post('', req.body, function(err, resM, body){
    res.send(body);
  })
})


app.post('/v2/login', function(req, res){
  var email = req.body.email;
  var pasword = req.body.password
  var query = 'q={"email":"' + email + '", "password":"' + pasword + '"}';
  console.log(query);

  clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "Usuarios?" + apiKey + "&" + query);
  //console.log(urlMLabRaiz + "Usuarios?" + apiKey + "&" + query);
  clienteMLabRaiz.get('', function(err, resM, body){
    if(!err){
      if(body.length == 1){
        res.status(200).send('Usuario logueado');
      }
      else{
        res.status(400).send('Usuario inexistente');
      }
    }
  })

})
